# Docker Deploy

Python3 script for container deployment

## How to use

```
usage: docker_deploy.py [-h] [-t TAG] {install,create,drop,start,stop,hosts,infos} ...

Deploy configured docker containers

positional arguments:
  {install,create,drop,start,stop,hosts,infos}
                        COMMAND to execute
    install             INSTALL PARSER
    create              CREATE PARSER
    drop                DROP PARSER
    start               START PARSER
    stop                STOP PARSER
    hosts               HOSTS PARSER
    infos               INFOS PARSER

optional arguments:
  -h, --help            show this help message and exit
  -t TAG, --tag TAG     Set tag to identify containers group

```

## amelioration

- use sudo inside scripts
- Enhance sudo usages

### Features

- Add config management
- Add interactive menu
- Add install docker commmand
- Add "adding to path" command
