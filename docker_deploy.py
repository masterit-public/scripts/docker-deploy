import shutil
import crypt
import re
import argparse
import os
import subprocess

# VARIABLES ------------------------------------------------------------------------------------------------------------

default_user = os.environ["USER"]
default_tag = "docker-deploy"
default_password = "p"
tmp_hosts_file = '/tmp/hosts'
hosts_file = '/etc/hosts'
hash_for_identification = "ea3fac1c22351b33cd203f7e66e68b99"

# MAIN PARSER ----------------------------------------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Deploy configured docker containers')
parser.set_defaults(which='main')
parser.add_argument('-t', '--tag',
                    metavar='TAG',
                    default=default_tag,
                    action='store',
                    help='Set tag to identify containers group')
# parser.add_argument('-i', '--interactive',
#                     action='store_true',
#                     help='Interactive menu (not yet dev)')

command_parsers = parser.add_subparsers(help='COMMAND to execute')

# INSTALL PARSER -------------------------------------------------------------------------------------------------------

command_parser_create = command_parsers.add_parser('install', help='INSTALL PARSER')
command_parser_create.set_defaults(which='install')

# CREATE PARSER --------------------------------------------------------------------------------------------------------

command_parser_create = command_parsers.add_parser('create', help='CREATE PARSER')
command_parser_create.set_defaults(which='create')
command_parser_create.add_argument('-u', '--user',
                                   metavar='USER',
                                   default=default_user,
                                   action="store",
                                   nargs='?',
                                   type=str,
                                   help='User to be created in docker container')
command_parser_create.add_argument('-p', '--password',
                                   metavar='PASSWORD',
                                   default=default_password,
                                   action="store",
                                   nargs='?',
                                   type=str,
                                   help='Password for the user')
command_parser_create.add_argument('--sudo-password',
                                   action="store_true",
                                   help='User need to enter his password for sudo command')
command_parser_create.add_argument('number',
                                   metavar='NUMBER',
                                   default=1,
                                   nargs='?',
                                   type=int,
                                   help='Number of nodes to be created')
command_parser_create.add_argument('names',
                                   metavar='NAME',
                                   nargs="*",
                                   type=str,
                                   help='Number of nodes to be created')

# DROP PARSER ----------------------------------------------------------------------------------------------------------

command_parser_drop = command_parsers.add_parser('drop', help='DROP PARSER')
command_parser_drop.set_defaults(which='drop')
command_parser_drop.add_argument('pattern',
                                 metavar='PATTERN',
                                 default='.*',
                                 type=str,
                                 nargs='?',
                                 help='Names of nodes to be drop')

# START PARSER ---------------------------------------------------------------------------------------------------------

command_parser_start = command_parsers.add_parser('start', help='START PARSER')
command_parser_start.set_defaults(which='start')

# STOP PARSER ----------------------------------------------------------------------------------------------------------

command_parser_stop = command_parsers.add_parser('stop', help='STOP PARSER')
command_parser_stop.set_defaults(which='stop')

# HOSTS PARSER ---------------------------------------------------------------------------------------------------------

command_parser_hosts = command_parsers.add_parser('hosts', help='HOSTS PARSER')
command_parser_hosts.set_defaults(which='hosts')
command_parser_hosts.add_argument('--domain',
                                  metavar='DOMAIN',
                                  type=str,
                                  nargs='?',
                                  help='Domain to add to the nodes in hosts file')

# INFOS PARSER ---------------------------------------------------------------------------------------------------------

command_parser_infos = command_parsers.add_parser('infos', help='INFOS PARSER')
command_parser_infos.set_defaults(which='infos')
command_parser_infos.add_argument('pattern',
                                  metavar='PATTERN',
                                  default='.*',
                                  type=str,
                                  nargs='?',
                                  help='Names of nodes to be drop')


# MAIN -------------------------------------------------------------------------------------------------------------

def main():
    print(args)
    menu = {
        'install': command_install,
        'create': command_create,
        'drop': command_drop,
        'start': command_start,
        'stop': command_stop,
        'hosts': command_hosts
    }

    return menu.get(args.which, command_infos)()


# COMMANDS -------------------------------------------------------------------------------------------------------------

def command_install():
    docker_install()
    return


def command_create():
    if args.drop_before_create is True:
        drop_nodes()

    if len(args.names) <= 0:
        create_nodes(generate_nodes_names())
    elif len(args.names) < args.number:
        print('    =X There is not enough names given for the nodes (' + str(
            len(args.names)) + ' given and ' + args.number + ' expected)')
    elif len(args.names) > args.number:
        print('    =X There is too much names given for the nodes (' + str(
            len(args.names)) + ' given and ' + args.number + ' expected)')
    else:
        create_nodes(args.names)


def command_drop():
    print(args)
    if args.remove_hosts_infos is True:
        undeploy_hosts_info()
    return drop_nodes(args.pattern)


def command_start():
    return start_nodes(args.pattern)


def command_stop():
    return stop_nodes(args.pattern)


def command_hosts():
    if args.undo:
        return undeploy_hosts_info()
    else:
        return deploy_hosts_infos(args.domain)


def command_infos():
    return infos_nodes(args.pattern)


# DOCKER ---------------------------------------------------------------------------------------------------------------

def docker_install():
    return


def docker_create(container_name):
    command_str = 'docker run -d --privileged --publish-all=true -v /srv/data:/srv/html -v ' \
                  '/sys/fs/cgroup:/sys/fs/cgroup:ro --name ' + container_name + ' -h ' + container_name + ' priximmo/buster-systemd-ssh '
    subprocess.run(command_str.split(), stdout=subprocess.PIPE)
    return 0


def docker_start(container_name):
    command = ['docker', 'start', container_name]
    return subprocess.run(command, stdout=subprocess.PIPE)


def docker_stop(container_name):
    command = ['docker', 'stop', container_name]
    return subprocess.run(command, stdout=subprocess.PIPE)


def docker_exec(container_name, command):
    command = ['docker', 'exec', container_name, '/bin/sh', '-c', command]
    subprocess.run(command, stdout=subprocess.PIPE)
    return 0


def docker_cp(container_name, src, dest=None):
    if dest is None:
        dest = src

    command = ['docker', 'cp', src, container_name + ':' + dest]
    subprocess.run(command, stdout=subprocess.PIPE)
    return 0


def docker_remove(container_name):
    command = ['docker', 'rm', '-f', container_name]
    subprocess.run(command, stdout=subprocess.PIPE)
    return


def docker_inspect(container_name, pattern):
    command = ['docker', 'inspect', '-f', pattern, container_name]
    result = subprocess.run(command, capture_output=True).stdout.decode('utf-8')
    return result.strip()


def docker_get_container_ip(container_name):
    return docker_inspect(container_name, "{{ .NetworkSettings.IPAddress }}")


def docker_get_container_id(container_name):
    return docker_inspect(container_name, "{{ .Id }}")[:12]


def get_container_name(node_name):
    return args.tag + '-' + node_name


# COUNT ----------------------------------------------------------------------------------------------------------------

def get_nodes_names(pattern=".*"):
    nodes_names = []

    containers = subprocess.check_output(['docker', 'ps', '-a'])

    lines = containers.decode('utf-8').splitlines()
    for nr, line in enumerate(lines):
        if nr > 0:
            container_name = (line.split().pop())
            if bool(re.search('^' + args.tag + '-', container_name)):
                node_name = re.sub('^' + args.tag + '-', '', container_name)
                if bool(re.search(pattern, node_name)):
                    nodes_names.append(node_name)

    return nodes_names


def count_nodes(pattern=".*"):
    return len(get_nodes_names(pattern))


def node_exists(node_name):
    return count_nodes('^' + node_name + '$') == 1


# CREATION -------------------------------------------------------------------------------------------------------------

def create_node(node_name):
    container_name = get_container_name(node_name)
    if node_exists(node_name):
        print('    =X Node ' + node_name + ' already exists')
        return 1
    else:
        docker_create(container_name)
        docker_exec(container_name, "useradd -m -p '$1$YCyoNmrS$24A7TRu3qAarRA7fM/X1y1' " + args.user)
        docker_exec(container_name, 'echo \"' + args.user + '  ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers')
        docker_exec(container_name, 'service ssh start')
        docker_exec(container_name, 'chsh -s /bin/bash ' + args.user)
        print('    => Node ' + node_name + ' created (container name : ' + container_name + ')')
    return 0


def create_nodes(nodes_names):
    print('---------------------------------------------------')
    print(' CREATE NODES')
    print('---------------------------------------------------\n')

    for node_name in nodes_names:
        create_node(node_name)
    print('    ---------------------------------------------------------')
    print('    => ' + str(len(nodes_names)) + ' nodes created\n')
    return 0


def generate_nodes_names():
    nodes_names = []
    node_num = 0

    while node_exists('node-' + str(node_num)):
        node_num += 1

    for x in range(node_num, node_num + args.number):
        nodes_names.append(str('node-' + str(x)))

    return nodes_names


# DROP -----------------------------------------------------------------------------------------------------------------

def drop_node(node_name):
    container_name = get_container_name(node_name)
    if not node_exists(node_name):
        print('    =X Node ' + node_name + ' doesn\'t exists')
        return 1
    else:
        docker_remove(container_name)
        print('    => Node ' + node_name + ' drop (container name : ' + container_name + ')')
    return 0


def drop_nodes(pattern=".*"):
    print('---------------------------------------------------')
    print(' DROP NODES')
    print('---------------------------------------------------\n')

    nodes_names = get_nodes_names(pattern)

    if count_nodes(pattern) <= 0:
        print('    =X There is no host matching the pattern "' + args.pattern + '" with the tag "' + args.tag + '"')
    else:
        for node_name in nodes_names:
            drop_node(node_name)
    print('    ---------------------------------------------------------')
    print('    => ' + str(len(nodes_names)) + ' nodes drop\n')


# START ----------------------------------------------------------------------------------------------------------------

def start_node(node_name):
    container_name = get_container_name(node_name)
    if not node_exists(node_name):
        print('    =X Node ' + node_name + ' doesn\'t exists')
        return 1
    else:
        docker_start(container_name)
        print('    => Node ' + node_name + ' started (container name : ' + container_name + ')')
    return 0


def start_nodes(pattern):
    print('---------------------------------------------------')
    print(' START NODES')
    print('---------------------------------------------------\n')

    nodes_names = get_nodes_names(pattern)

    if count_nodes(pattern) <= 0:
        print('    =X There is no host matching the pattern "' + args.pattern + '" with the tag "' + args.tag + '"')
    else:
        for node_name in nodes_names:
            start_node(node_name)
    print('    ---------------------------------------------------------')
    print('    => ' + str(len(nodes_names)) + ' nodes started\n')


# STOP -----------------------------------------------------------------------------------------------------------------

def stop_node(node_name):
    container_name = get_container_name(node_name)
    if not node_exists(node_name):
        print('    =X Node ' + node_name + ' doesn\'t exists')
        return 1
    else:
        docker_stop(container_name)
        print('    => Node ' + node_name + ' stoped (container name : ' + container_name + ')')
    return 0


def stop_nodes(pattern):
    print('---------------------------------------------------')
    print(' STOP NODES')
    print('---------------------------------------------------\n')

    nodes_names = get_nodes_names(pattern)

    if count_nodes(pattern) <= 0:
        print('    =X There is no host matching the pattern "' + args.pattern + '" with the tag "' + args.tag + '"')
    else:
        for node_name in nodes_names:
            stop_node(node_name)
    print('    ---------------------------------------------------------')
    print('    => ' + str(len(nodes_names)) + ' nodes stoped\n')


# INFOS ----------------------------------------------------------------------------------------------------------------

def infos_node(node_name):
    container_name = get_container_name(node_name)
    print('    => ' + node_name +
          ':   ' + container_name +
          '   ' + docker_get_container_ip(container_name) +
          '   ' + docker_get_container_id(container_name) + '')
    return 0


def infos_nodes(pattern):
    print('---------------------------------------------------')
    print(' INFOS NODES')
    print('---------------------------------------------------\n')

    nodes_names = get_nodes_names(pattern)

    if count_nodes(pattern) <= 0:
        print('    =X There is no host matching the pattern "' + args.pattern + '" with the tag "' + args.tag + '"')
    else:
        for node_name in nodes_names:
            infos_node(node_name)
    print('    ---------------------------------------------------------')
    print('    => ' + str(len(nodes_names)) + ' nodes\n')


# HOSTS FILE -----------------------------------------------------------------------------------------------------------

def generate_host_infos(node_name, domain=None):
    container_name = get_container_name(node_name)
    host_infos = docker_get_container_ip(container_name) + \
                 ' ' + \
                 container_name + \
                 ' ' + \
                 node_name

    if domain is not None:
        host_infos += ' ' + node_name + '.' + domain

    return host_infos


def generate_hosts_infos(domain=None):
    hosts_infos = []

    nodes_names = get_nodes_names()

    for node_name in nodes_names:
        hosts_infos.append(generate_host_infos(node_name, domain=domain))
    return hosts_infos


def create_tmp_hosts_file(hosts_infos):
    file = open(tmp_hosts_file, "w+")
    for host_infos in hosts_infos:
        file.write(host_infos)
    return file.close()


def delete_tmp_hosts_file():
    return os.remove(tmp_hosts_file)


def deploy_hosts_infos_on_host(node_name, domain=None):
    container_name = get_container_name(node_name)
    docker_cp(container_name, tmp_hosts_file)
    docker_exec(container_name, 'cat ' + tmp_hosts_file + '| tee -a ' + hosts_file)


    print("    => Hosts infos added on node " + node_name + ' (hostname = ' + node_name + '.' + domain +')')
    return 0


def deploy_hosts_infos_on_hosts(domain=None):
    nodes_names = get_nodes_names()

    for node_name in nodes_names:
        deploy_hosts_infos_on_host(node_name, domain)
    print('    ---------------------------------------------------------')
    print("    => Hosts infos added on " + str(len(nodes_names)) + " nodes and localhost")

    return 0


def add_hosts_infos_on_localhost(hosts_infos):
    file = open(hosts_file, "a+")

    file.write(
        "# START OF GENERATED HOSTS INFOS FROM docker_deploy.py SCRIPT (HASH : START_" + hash_for_identification + ")\n")

    for host_infos in hosts_infos:
        file.write(host_infos + "\n")

    file.write(
        "# END OF GENERATED HOSTS INFOS FROM docker_deploy.py SCRIPT (HASH : END_" + hash_for_identification + ")\n")

    file.close()

    print("    => Hosts infos added on localhost\n")


def remove_hosts_infos_on_localhost():

    tmp_file = hosts_file + '.tmp'
    line_deletation = False
    with open(hosts_file, 'r') as read_obj, open(tmp_file, 'w') as write_obj:
        for line in read_obj:
            if re.search("START_" + hash_for_identification, line):
                line_deletation = True
            if not line_deletation:
                write_obj.write(line)
            if re.search("END_" + hash_for_identification, line):
                line_deletation = False


    os.remove(hosts_file)
    os.rename(tmp_file, hosts_file)
    print("    => Hosts infos removed on localhost\n")


def deploy_hosts_infos(domain):
    print('---------------------------------------------------')
    print(' HOSTS FILE DEPLOYMENT')
    print('---------------------------------------------------\n')

    hosts_infos = generate_hosts_infos(domain)

    add_hosts_infos_on_localhost(hosts_infos)

    create_tmp_hosts_file(hosts_infos)
    deploy_hosts_infos_on_hosts(domain)
    delete_tmp_hosts_file()



def undeploy_hosts_info():
    print('---------------------------------------------------')
    print(' HOSTS FILE UNDEPLOYMENT')
    print('---------------------------------------------------\n')
    remove_hosts_infos_on_localhost()


# MAIN -----------------------------------------------------------------------------------------------------------------

args = parser.parse_args()
main()
